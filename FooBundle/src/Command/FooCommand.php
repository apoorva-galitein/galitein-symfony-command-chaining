<?php

declare(strict_types=1);

namespace Galitein\FooBundle\Command;

use Galitein\ChainBundle\Traits\ChainLoggerTrait;
use Symfony\Component\Console\Command\Command;

class FooCommand extends Command
{
    use ChainLoggerTrait;

    public function __construct(string $name = null)
    {
        parent::__construct($name);
    }

    public function configure()
    {
        $this->setName('foo:hello');
    }
}
