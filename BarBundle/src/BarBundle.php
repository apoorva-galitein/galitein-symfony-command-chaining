<?php

namespace Galitein\BarBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class BarBundle
 * @package Galitein\BarBundle
 */
class BarBundle extends Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }
}
