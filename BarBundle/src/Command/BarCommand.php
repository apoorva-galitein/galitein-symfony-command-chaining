<?php

declare(strict_types=1);

namespace Galitein\BarBundle\Command;

use Galitein\ChainBundle\Traits\ChainLoggerTrait;
use Symfony\Component\Console\Command\Command;

/**
 * Class BarCommand
 * @package Galitein\BarBundle\Command
 */
class BarCommand extends Command
{
    use ChainLoggerTrait;

    public function __construct(string $name = null)
    {
        parent::__construct($name);
    }

    public function configure()
    {
        $this->setName('bar:hi');
    }
}
