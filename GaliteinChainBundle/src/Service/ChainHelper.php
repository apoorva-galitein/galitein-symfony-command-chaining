<?php

namespace Galitein\ChainBundle\Service;

/**
 * Class ChainHelper
 * @package Galitein\ChainBundle\Service
 */
class ChainHelper
{
    private array $commandsHierarchy = [];
    private array $commands = [];

    public function addCommand(string $command, ?string $parent): void
    {
        if (null === $parent && !isset($this->commandsHierarchy[$command])) {
            $this->commandsHierarchy[$command] = [];
        } else {
            $this->commandsHierarchy[$parent][] = $command;
        }

        $this->commands[] = $command;
    }

    public function getCommands(): array
    {
        return [
            'commandsHierarchy' => $this->commandsHierarchy,
            'commands' => $this->commands,
        ];
    }

    public function validate(string $command): bool
    {
        if (!in_array($command, $this->commands, true)) {
            return true;
        }

        return in_array($command, array_keys($this->commandsHierarchy));
    }

    /**
     * Get children commands by parent comamnd.
     *
     * @param string $command
     * @return array|mixed
     */
    public function getChildrenCommandsByParent(string $command)
    {
        $commandsHierarchy = $this->getCommands()['commandsHierarchy'];

        if (isset($commandsHierarchy[$command])) {
            return $commandsHierarchy[$command];
        }

        return [];
    }

    /**
     * Search whether the command is top level command.
     *
     * @param string $command
     * @return bool
     */
    public function isTopLevelCommand(string $command)
    {
        $commandsHierarchy = $this->getCommands()['commandsHierarchy'];

        if (isset($commandsHierarchy[$command])) {
            return true;
        }

        return false;
    }

    /**
     * Search parent command by child command.
     *
     * @param $array
     * @param $findWord
     *
     * @return string
     */
    public function searchParentCommand(array $array, string $wordToFind)
    {
        $parent = '';

        foreach ($array as $k => $v) {
            if (in_array($wordToFind, $array[$k])) {
                $parent = $k;
                break;
            }
        }

        return $parent;
    }
}
