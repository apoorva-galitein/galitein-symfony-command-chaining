<?php

namespace Galitein\ChainBundle;

use Galitein\ChainBundle\CompilerPass\ChainCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class GaliteinChainBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container->addCompilerPass(new ChainCompilerPass());
    }

    public function getPath(): string
    {
        return \dirname(__DIR__);
    }
}
