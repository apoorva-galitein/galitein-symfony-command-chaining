<?php

namespace Galitein\ChainBundle\Traits;

use Galitein\ChainBundle\Service\ChainHelper;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * If you are making command chain, reuse methods from this trait.
 */
trait ChainLoggerTrait
{
    /** @var LoggerInterface */
    private $logger;

    /** @var ChainHelper */
    private $chainHelper;

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param ChainHelper $chainHelper
     */
    public function setChainHelper(ChainHelper $chainHelper)
    {
        $this->chainHelper = $chainHelper;
    }

    /**
     * If you are building command chain use this common execute method.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $commandName = $this->getName();

        $subCommandsString = '';
        $parentCommandName = '';
        if (true === $this->chainHelper->isTopLevelCommand($commandName)) {
            $subCommandsString = implode(", ", $this->chainHelper->getChildrenCommandsByParent($commandName));
        } else {
            $parentCommandName = $this->chainHelper->searchParentCommand($this->chainHelper->getCommands()['commandsHierarchy'], $commandName);
        }

        $message = '';
        if (empty($subCommandsString)) {
            $message = sprintf('SUCCESS: Run %s registered as a member of %s command chain', $commandName, $parentCommandName);
        } else {
            $message = sprintf('SUCCESS: Parent command %s is executing and its child commands %s will execute after it', $commandName, $subCommandsString);
        }

        $io->success($message);
        $this->logger->info($message);

        return Command::SUCCESS;
    }
}
