<?php

namespace Galitein\ChainBundle\EventSubscriber;

use Galitein\ChainBundle\Service\ChainHelper;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ConsoleCommandSubscriber
 * @package Galitein\ChainBundle\EventSubscriber
 */
class ConsoleCommandSubscriber implements EventSubscriberInterface
{
    public function __construct(private ChainHelper $chainHelper)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ConsoleCommandEvent::class => 'onConsoleCommand',
            ConsoleTerminateEvent::class => 'onConsoleTerminate'
        ];
    }

    public function onConsoleCommand(ConsoleCommandEvent $event)
    {
        if (!$this->chainHelper->validate($event->getCommand()->getName())) {
            $event->getOutput()->writeln(['<error>ERROR: You can not run this command directly.</error>']);
            $event->stopPropagation();
            exit();
        }
    }

    public function onConsoleTerminate(ConsoleTerminateEvent $event)
    {
        $application = $event->getCommand()->getApplication();
        $commandsToBeExecute = $this->chainHelper->getCommands()['commandsHierarchy'][$event->getCommand()->getName()] ?? [];
        foreach ($commandsToBeExecute as $commandToBeExecute) {
            $command = $application->find($commandToBeExecute);
            $command->run($event->getInput(), $event->getOutput());
        }
    }
}
