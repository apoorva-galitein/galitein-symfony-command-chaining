<?php

namespace Galitein\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class SportsCommandTest extends KernelTestCase
{
    /**
     * Test the execution of the command
     */
    public function testSuccess()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('chain:sports');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('SUCCESS', $output);
    }
}
