<?php

namespace Galitein\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class FootballCommandTest extends KernelTestCase
{
    /**
    * Test the execution of the command
    */
    public function testError()
    {
        // @todo: Do not use shell_exec. Need improvement.
        $result = shell_exec("php bin/console chain:football");

        $this->assertStringContainsString('ERROR', $result);
    }
}
