[Read requirement here ](https://github.com/mbessolov/test-tasks/blob/master/7.md)

Add this in your main Symfony project's composer.json
```
    "repositories":[
        {
            "type": "git",
            "url": "https://bitbucket.org/apoorva-galitein/galitein-symfony-command-chaining"
        }
    ]
```

In composer.json require section add:
```
"apoorva-galitein/galitein-symfony-command-chaining": "dev-master",
```

OR Install that bundle with
```
composer require apoorva-galitein/galitein-symfony-command-chaining:dev-master
```

Let's see command chaining functionality:

Suppose top-level command is chain:sports and its sublevel commands are chain:cricket and chain:football.

Then chain:sports will run directly but chain:cricket and chain:football with not run directly.
To achieve that functionality, you need to register your bundles in services.yaml.

Notice, in top-level command chain:sports we are not taking the "parent" key under properties.

```
    Galitein\ChainBundle\Command\SportsCommand:
        calls:
            - setLogger: ['@logger']
        tags: ['app.chain']
        properties:
            command: 'chain:sports'
```

Here is child command of sports. So here we have added chain:sports in parent key (under properties). You can do this in other bundles too.

```
    Galitein\ChainBundle\Command\CricketCommand:
        calls:
            - setLogger: ['@logger']
        tags: ['app.chain']
        properties:
            command: 'chain:cricket'
            parent: 'chain:sports'

    Galitein\ChainBundle\Command\FootballCommand:
        calls:
            - setLogger: ['@logger']
        tags: ['app.chain']
        properties:
            command: 'chain:football'
            parent: 'chain:sports'
```

- I have created FooBundle and BarBundle. After you install this bundle you will see the command foo:hello will run directly. But bar:hi will not run directly.

##### Coding guide:
Please see the following files:

- Compiler pass: /galitein-symfony-command-chaining/GaliteinChainBundle/src/CompilerPass

- I have moved a few common methods of commands in this trait: galitein-symfony-command-chaining/GaliteinChainBundle/src/Traits/ChainLoggerTrait.
So you can use this trait.
